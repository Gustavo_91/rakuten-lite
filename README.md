# Project Title

Rakuten Lite. A humble version of the main app

## Getting Started

1. Open Rakuten Lite in Xcode 11 (Open the workspace, not the project)
2. Build and Run the **Rakuten Lite** target in a device or simulator with iOS >= 12.0

### Prerequisites

* Xcode 11
* Swift 5.1

## Author

* **Gustavo Pirela**

## Notes

I couldn't test the app in a device with iOS 12 because I only have one with iOS 13


