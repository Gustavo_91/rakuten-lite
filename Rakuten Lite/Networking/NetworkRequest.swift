//
//  NetworkRequest.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 23/11/2019.
//  Copyright © 2019 me. All rights reserved.
//

import Foundation

class NetworkRequest {
    let session = URLSession(configuration: .ephemeral, delegate: nil, delegateQueue: nil)
    let url: URLRequest
    
    init(request: URLRequest) {
        self.url = request
    }
    
    func execute(completion: @escaping (Data?) -> Void) {
        let task = session.dataTask(with: url) {data, response, error in
            completion(data)
        }
        task.resume()
    }
    
}
