//
//  StateController.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 27/11/2019.
//  Copyright © 2019 me. All rights reserved.
//

import Foundation

class StateController {
    static var items: [ItemType: [MovieItem]] = [:]
    static var movie: Movie?
    
    static var selectedSpec: (type: ItemType, row: Int)?
    
    static var selectedMovie: MovieItem? {
        guard let spec = selectedSpec else {
            return nil
        }
        
        return items[spec.type]?[spec.row]
    }

    static subscript(section section: Int) -> ItemType {
        let sortedKeys = Array(items.keys).sorted(by: <)
        return sortedKeys[section]
    }
}
