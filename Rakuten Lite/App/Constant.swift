//
//  Constant.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 26/11/2019.
//  Copyright © 2019 me. All rights reserved.
//

import UIKit

struct Constant {
    struct Height {
        static let itemsList: CGFloat = Constant.Height.itemCell + 20
        static let listHeader: CGFloat = 30
        static let itemCell: CGFloat = 290
        static let trailerCell: CGFloat = 500
        static let descriptionCell: CGFloat = 250
        static let genresList: CGFloat = Constant.Height.genreCell + 20
        static let castingList: CGFloat = Constant.Height.castingCell + 20
        static let genreCell: CGFloat = 40
        static let castingCell: CGFloat = 190
        static let movieHeader: CGFloat = 45
    }
    
    struct Width {
        static let itemCell: CGFloat = 190
        static let genreCell: CGFloat = 170
        static let castingCell: CGFloat = 130
    }
}
