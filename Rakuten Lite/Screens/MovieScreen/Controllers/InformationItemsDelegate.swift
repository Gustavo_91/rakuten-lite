//
//  InformationItemsDelegate.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 03/12/2019.
//  Copyright © 2019 me. All rights reserved.
//

import UIKit

class InformationItemsDelegate: NSObject {
    var movieSection: MovieSection?
}

extension InformationItemsDelegate: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch movieSection {
        case .genres:
            return CGSize(width: Constant.Width.genreCell, height: Constant.Height.genreCell)
        case .casting:
            return CGSize(width: Constant.Width.castingCell, height: Constant.Height.castingCell)
        default:
            return CGSize(width: 0, height: 0)
        }
 
    }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
}
