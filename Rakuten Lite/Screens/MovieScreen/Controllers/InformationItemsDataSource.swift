//
//  InformationItemsDataSource.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 03/12/2019.
//  Copyright © 2019 me. All rights reserved.
//

import UIKit
import Nuke

class InformationItemsDataSource: NSObject {
    var movieSection: MovieSection?
    var genres: [String]?
    var casting: [People]?
}

extension InformationItemsDataSource: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch movieSection {
        case .genres:
            return genres != nil ? genres!.count : 0
        case .casting:
            return casting != nil ? casting!.count : 0
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch movieSection {
        case .genres:
            return setGenresCell(collectionView: collectionView, indexPath: indexPath)
        case .casting:
            return setCastingCell(collectionView: collectionView, indexPath: indexPath)
        default:
            return UICollectionViewCell()
        }
    }
}

private extension InformationItemsDataSource {
    func setGenresCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: GenreCell.self), for: indexPath) as! GenreCell
        let viewModel = GenreCell.ViewModel(genre: genres?[indexPath.row])
        cell.viewModel = viewModel
        return cell
    }
    
    func setCastingCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: CastingCell.self), for: indexPath) as! CastingCell
        let viewModel = CastingCell.ViewModel(name: casting?[indexPath.row].name)
        cell.viewModel = viewModel
        if let url = casting?[indexPath.row].photoUrl { Nuke.loadImage(with: url, into: cell.imageView) }
        return cell
    }
    
}
