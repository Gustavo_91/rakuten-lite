//
//  MovieDataSource.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 28/11/2019.
//  Copyright © 2019 me. All rights reserved.
//

import UIKit
import Nuke

class MovieDataSource: NSObject {
    
}

extension MovieDataSource: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return StateController.movie != nil ? 1 : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = MovieSection(rawValue: indexPath.section) else {
            return UITableViewCell()
        }
        
        switch section {
        case .trailer:
            return setTrailerCell(tableView: tableView)
        case .description:
            return setDescriptionCell(tableView: tableView)
        case .genres, .casting:
            return setInformationCell(tableView: tableView, section: section)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return StateController.movie != nil ? MovieSection.allCases.count : 0
    }
    
}

private extension MovieDataSource {
    func setTrailerCell(tableView: UITableView) -> UITableViewCell {
        let movie = StateController.movie
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TrailerCell.self)) as! TrailerCell
        if let url = movie?.snapshotUrl {
            Nuke.loadImage(with: url, into: cell.movieImageView)
        }
    
        return cell
    }
    
    func setDescriptionCell(tableView: UITableView) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: DescriptionCell.self)) as! DescriptionCell
        let movie = StateController.movie
        let viewModel = movie != nil ? DescriptionCell.ViewModel(movie: movie!) : DescriptionCell.ViewModel()
        cell.viewModel = viewModel
        
        return cell
    }
    
    func setInformationCell(tableView: UITableView, section: MovieSection) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: InformationCell.self)) as! InformationCell
        var viewModel = InformationCell.ViewModel(movie: StateController.movie)
        viewModel.movieSection = section
        cell.viewModel = viewModel
        
        return cell
    }
}

enum MovieSection: Int, CaseIterable {
    case trailer
    case description
    case genres
    case casting
    
    var title: String {
        switch self {
        case .trailer, .description:
            return ""
        case .genres:
            return "Géneros"
        case .casting:
            return "Reparto"
        }
    }
}
