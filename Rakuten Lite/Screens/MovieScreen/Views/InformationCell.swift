//
//  InformationCell.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 03/12/2019.
//  Copyright © 2019 me. All rights reserved.
//

import UIKit

class InformationCell: UITableViewCell {
    @IBOutlet private weak var collectionView: UICollectionView!
    let dataSource = InformationItemsDataSource()
    let delegate = InformationItemsDelegate()
    
    var viewModel = ViewModel() {
        didSet {
            dataSource.genres = viewModel.genres
            dataSource.casting = viewModel.casting
            dataSource.movieSection = viewModel.movieSection
            delegate.movieSection = viewModel.movieSection
            collectionView.reloadData()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.allowsSelection = false
        collectionView.dataSource = dataSource
        collectionView.delegate = delegate
    }

}
