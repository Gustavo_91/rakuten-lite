//
//  DescriptionCell.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 29/11/2019.
//  Copyright © 2019 me. All rights reserved.
//

import UIKit

class DescriptionCell: UITableViewCell {
    @IBOutlet private weak var scoreLabel: UILabel!
    @IBOutlet private weak var votesLabel: UILabel!
    @IBOutlet private weak var yearLabel: UILabel!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var plotLabel: UILabel!
    
    var viewModel: ViewModel = ViewModel() {
        didSet {
            scoreLabel.text = viewModel.score
            votesLabel.text = viewModel.votes
            yearLabel.text = viewModel.year
            titleLabel.text = viewModel.title
            plotLabel.text = viewModel.plot
        }
    }
}
