//
//  GenreCell.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 03/12/2019.
//  Copyright © 2019 me. All rights reserved.
//

import UIKit

class GenreCell: UICollectionViewCell {
    @IBOutlet private weak var genreLabel: UILabel!
    @IBOutlet private weak var customBackground: DesignableView!
    
    var viewModel = ViewModel() {
        didSet {
            genreLabel.text = viewModel.genre
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.customBackground.cornerRadius = 17
    }
}
