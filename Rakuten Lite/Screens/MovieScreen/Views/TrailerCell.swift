//
//  TrailerCell.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 28/11/2019.
//  Copyright © 2019 me. All rights reserved.
//

import UIKit

class TrailerCell: UITableViewCell {
    @IBOutlet weak var movieImageView: UIImageView!
    
    var viewModel: ViewModel = ViewModel()  {
        didSet {
            movieImageView.image = viewModel.image
        }
    }
}
