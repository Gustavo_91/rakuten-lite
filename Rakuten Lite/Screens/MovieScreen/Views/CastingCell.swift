//
//  CastingCell.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 04/12/2019.
//  Copyright © 2019 me. All rights reserved.
//

import UIKit

class CastingCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    var viewModel = ViewModel() {
        didSet {
            self.label.text = viewModel.name
        }
    }
}
