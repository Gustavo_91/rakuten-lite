//
//  MovieHeader.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 03/12/2019.
//  Copyright © 2019 me. All rights reserved.
//

import UIKit

class MovieHeader: UIView {
    @IBOutlet private weak var label: UILabel!

    var title: String = "" {
        didSet { label.text = title }
    }

}
