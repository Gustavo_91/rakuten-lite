//
//  GenreCellViewModel.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 03/12/2019.
//  Copyright © 2019 me. All rights reserved.
//

import UIKit

extension GenreCell {
    struct ViewModel {
        var genre: String?
        
        init(genre: String?) {
            self.genre = genre?.uppercased()
        }
    }
}

extension GenreCell.ViewModel {
    init() {
        self.genre = nil
    }
}
