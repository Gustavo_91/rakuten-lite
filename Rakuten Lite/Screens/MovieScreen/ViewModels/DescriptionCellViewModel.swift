//
//  InformationCellViewModel.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 29/11/2019.
//  Copyright © 2019 me. All rights reserved.
//

import UIKit

extension DescriptionCell {
    struct ViewModel {
        let votes: String
        let score: String
        let year: String
        let title: String
        let plot: String
    }
}

extension DescriptionCell.ViewModel {
    init() {
        self.votes = ""
        self.score = ""
        self.year = ""
        self.title = ""
        self.plot = ""
    }
    
    init(movie: Movie) {
        self.votes = movie.scores.count > 0 ? (movie.scores[1].votes ?? "") : ""
        self.score = movie.scores.count > 0 ? String(movie.scores[1].score) : ""
        self.year = String(movie.year)
        self.title = movie.title
        self.plot = movie.plot
    }
}
