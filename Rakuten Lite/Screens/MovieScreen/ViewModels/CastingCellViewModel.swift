//
//  CastingCellViewModel.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 04/12/2019.
//  Copyright © 2019 me. All rights reserved.
//

import UIKit

extension CastingCell {
    struct ViewModel {
        var name: String?
    }
}
