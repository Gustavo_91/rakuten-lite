//
//  GenresListCellViewModel.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 03/12/2019.
//  Copyright © 2019 me. All rights reserved.
//

import UIKit

extension InformationCell {
    struct ViewModel {
        var movieSection: MovieSection?
        var genres: [String]?
        var casting: [People]?
    }
}

extension InformationCell.ViewModel {
    init(movie: Movie?) {
        self.genres = movie?.genres
        self.casting = movie?.casting
    }
}
