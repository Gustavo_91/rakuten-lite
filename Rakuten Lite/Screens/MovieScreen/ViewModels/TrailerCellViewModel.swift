//
//  TrailerCellViewModel.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 28/11/2019.
//  Copyright © 2019 me. All rights reserved.
//

import UIKit

extension TrailerCell {
    struct ViewModel {
        var image: UIImage?
    }
}
