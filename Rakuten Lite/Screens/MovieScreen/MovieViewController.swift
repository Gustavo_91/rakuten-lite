//
//  ScreenViewController.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 28/11/2019.
//  Copyright © 2019 me. All rights reserved.
//

import UIKit
import AVKit

class MovieViewController: UIViewController {
    @IBOutlet private weak var tableView: UITableView!
    let dataSource = MovieDataSource()

    override func viewDidLoad() {
        super.viewDidLoad()
        modalPresentationStyle = .fullScreen
        setTableView()
        fetchMovie()
    }
    
    @IBAction func presentTrailer(_ sender: UIButton) {
        fetchTrailer()
    }
    
}

extension MovieViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = MovieSection.init(rawValue: indexPath.section)
        switch section {
        case .trailer:
            return Constant.Height.trailerCell
        case .description:
            return Constant.Height.descriptionCell
        case .genres:
            return Constant.Height.genresList
        case .casting:
            return Constant.Height.castingList
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let section = MovieSection(rawValue: section)
        let header = UINib.init(nibName: "MovieHeader", bundle: nil).instantiate(withOwner: nil, options: nil).first as! MovieHeader
        header.title = section?.title ?? ""
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let section = MovieSection(rawValue: section)
        switch section {
        case .genres, .casting:
            return Constant.Height.movieHeader
        default:
            return 0
        }
    }
}

private extension MovieViewController {
    func setTableView() {
        tableView.allowsSelection = false
        tableView.dataSource = dataSource
        tableView.delegate = self
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .yellow
        tableView.refreshControl = refreshControl
        tableView.contentOffset = CGPoint(x:0, y:-refreshControl.frame.size.height)
        tableView.refreshControl?.beginRefreshing()
    }
    
    func fetchMovie() {
        guard let id = StateController.selectedMovie?.id, let urlRequest = Movie.makeRequestWith(id: id) else {
            return
        }
        
        let request = NetworkRequest(request: urlRequest)
        request.execute { [weak self] data in
            guard let data = data else { return }
            self?.decode(data)
        }
    }
    
    func decode(_ data: Data) {
        let decoder = JSONDecoder()
        let movie = try? decoder.decode(Movie.self, from: data)
        StateController.movie = movie
       
        DispatchQueue.main.async {
            self.tableView.refreshControl?.endRefreshing()
            self.tableView.reloadData()
        }
    }
    
    func fetchTrailer() {
        guard let id = StateController.movie?.id, let urlRequest = Trailer.makeRequestWith(id: id) else {
            return
        }

        let request = NetworkRequest(request: urlRequest)
        request.execute { data in
            guard let data = data else { return }
            self.decodeTrailer(data)
        }
    }

    func decodeTrailer(_ data: Data) {
        let decoder = JSONDecoder()
        guard let trailer = try? decoder.decode(Trailer.self, from: data) else { return }
        let player = AVPlayer(url: trailer.url)

        DispatchQueue.main.async {
            let controller = AVPlayerViewController()
            controller.player = player
            self.present(controller, animated: true, completion: nil)
            player.play()
        }
    }
}

