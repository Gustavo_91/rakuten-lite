//
//  ListDataSource.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 26/11/2019.
//  Copyright © 2019 me. All rights reserved.
//

import UIKit

class ListDataSource: NSObject {

}

extension ListDataSource: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return StateController.items.count == 0 ? 0 : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ListCell.self)) as! ListCell
        let sectionType = StateController[section: indexPath.section]
        var viewModel = ListCell.ViewModel(items: StateController.items[sectionType] ?? [])
        viewModel.type = sectionType
        cell.viewModel = viewModel
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return StateController.items.count
    }

}
