//
//  ItemsDelegate.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 26/11/2019.
//  Copyright © 2019 me. All rights reserved.
//

import UIKit

class ItemsDelegate: NSObject {
    
}

extension ItemsDelegate: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: Constant.Width.itemCell, height: Constant.Height.itemCell)
    }
        
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dataSource = collectionView.dataSource as! ItemsDataSource
        guard let type = dataSource.type else { return }
        let row = indexPath.row
        StateController.selectedSpec = (type, row)
    }
}

