//
//  ItemsDataSource.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 26/11/2019.
//  Copyright © 2019 me. All rights reserved.
//

import UIKit
import Nuke

class ItemsDataSource: NSObject {
    var items: [MovieItem] = []
    var type: ItemType?
}

extension ItemsDataSource: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ItemCell.self), for: indexPath) as! ItemCell
        let viewModel = ItemCell.ViewModel(item: items[indexPath.row])
        cell.viewModel = viewModel
        Nuke.loadImage(with: items[indexPath.row].artworkUrl, into: cell.imageView)
        return cell
    }
}
