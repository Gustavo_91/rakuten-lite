//
//  ListHeader.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 27/11/2019.
//  Copyright © 2019 me. All rights reserved.
//

import UIKit

class ListHeader: UIView {
    @IBOutlet private weak var label: UILabel!
    
    var text: String = ""  {
        didSet { label.text = text }
    }
}
