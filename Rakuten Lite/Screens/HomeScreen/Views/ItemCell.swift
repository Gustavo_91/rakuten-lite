//
//  ItemCell.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 26/11/2019.
//  Copyright © 2019 me. All rights reserved.
//

import UIKit

class ItemCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet private weak var scoreLabel: UILabel!
    @IBOutlet private weak var votesLabel: UILabel!
    
    var viewModel: ViewModel = ViewModel() {
        didSet {
            imageView.image = viewModel.image
            scoreLabel.text = viewModel.score
            votesLabel.text = viewModel.votes
        }
    }
}
