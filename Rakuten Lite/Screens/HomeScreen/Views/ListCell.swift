//
//  TableViewCell.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 26/11/2019.
//  Copyright © 2019 me. All rights reserved.
//

import UIKit

class ListCell: UITableViewCell {
    @IBOutlet private weak var collectionView: UICollectionView!
    let dataSource = ItemsDataSource()
    var delegate = ItemsDelegate()
    
    var viewModel = ViewModel(items: []) {
        didSet {
            dataSource.items = viewModel.items
            dataSource.type = viewModel.type
            collectionView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.dataSource = dataSource
        collectionView.delegate = delegate
    }
}
