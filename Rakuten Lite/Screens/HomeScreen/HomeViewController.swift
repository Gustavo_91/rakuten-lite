//
//  HomeViewController.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 26/11/2019.
//  Copyright © 2019 me. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet weak private var tableView: UITableView!
    let listDataSource = ListDataSource()

    override func viewDidLoad() {
        super.viewDidLoad()
        setTableView()
        fetchItems()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        StateController.movie = nil
    }
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constant.Height.itemsList
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UINib(nibName: "ListHeader", bundle: nil).instantiate(withOwner: nil, options: nil).first
        let headerView = view as? ListHeader
        let sectionType = StateController[section: section]
        headerView?.text = sectionType.title
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Constant.Height.listHeader
    }
}

private extension HomeViewController {
    func setTableView() {
        tableView.allowsSelection = false
        tableView.dataSource = listDataSource
        tableView.delegate = self
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .yellow
        tableView.refreshControl = refreshControl
        tableView.contentOffset = CGPoint(x:0, y:-refreshControl.frame.size.height)
        tableView.refreshControl?.beginRefreshing()
    }
    
    func fetchItems() {
        ItemType.allCases.forEach { type in
            guard let urlRequest = type.makeRequestWith(page: 1) else { return }
            let request = NetworkRequest(request: urlRequest)
            request.execute { [weak self] data in
                if let data = data {
                    self?.decode(data, type: type)
                }
            }
        }
    }
    
    func decode(_ data: Data, type: ItemType) {
        let decoder = JSONDecoder()
        guard let wrapper = try? decoder.decode(Wrapper.self, from: data) else { return }
        for page in 1...wrapper.pages {
            guard let urlRequest = type.makeRequestWith(page: page) else { return }
            let request = NetworkRequest(request: urlRequest)
            request.execute { [weak self] data in
                if let data = data {
                    let decoder = JSONDecoder()
                    guard let wrapper = try? decoder.decode(Wrapper.self, from: data) else { return }
                    self?.updateItems(wrapper: wrapper, type: type)
                    if page == wrapper.pages { self?.reloadUI(type: type) }
                }
            }
        }
    }
    
    func updateItems(wrapper: Wrapper, type: ItemType) {
         if StateController.items.keys.contains(type) {
             StateController.items[type]?.append(contentsOf: wrapper.items)
         } else {
             StateController.items[type] = wrapper.items
         }
    }
    
    func reloadUI(type: ItemType) {
        if StateController.items.count == ItemType.allCases.count {
            DispatchQueue.main.async {
                self.tableView.refreshControl?.endRefreshing()
                self.tableView.reloadData()
            }
        }
    }
}
