//
//  ListCellViewModel.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 29/11/2019.
//  Copyright © 2019 me. All rights reserved.
//

import UIKit

extension ListCell {
    struct ViewModel {
        let items: [MovieItem]
        var type: ItemType?
    }
}
