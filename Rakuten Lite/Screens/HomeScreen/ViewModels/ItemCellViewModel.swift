//
//  ItemCellViewModel.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 29/11/2019.
//  Copyright © 2019 me. All rights reserved.
//

import UIKit

extension ItemCell {
    struct ViewModel {
        var image: UIImage?
        let score: String
        let votes: String
    }
}

extension ItemCell.ViewModel {
    init(item: MovieItem) {
        self.score = item.score != nil ? String(item.score!) : ""
        self.votes = item.votes ?? ""
    }
    
    init() {
        self.score = ""
        self.votes = ""
    }
}
