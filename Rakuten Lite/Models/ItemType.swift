//
//  ItemType.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 27/11/2019.
//  Copyright © 2019 me. All rights reserved.
//

import Foundation

enum ItemType: Int, CaseIterable {
    case premiere
    case free
    case story
    case subscription
    case rent
    case buy
    case cartoon
    case princess
    case missed
    
    var title: String {
        switch self {
        case .premiere:
            return "Estrenos Imprescindibles"
        case .free:
            return "Peliculas Gratis"
        case .story:
            return "Free - Rakuten TV Stories"
        case .subscription:
            return "Lo mejor de tu Subscripción"
        case .rent:
            return "Películas mas vistas en Alquiler"
        case .buy:
            return "Películas populares en Compra"
        case .cartoon:
            return "Aventuras de Animación en Alquiler"
        case .princess:
            return "Princesas Disney"
        case .missed:
            return "Si te perdiste"
        }
    }
    
    var path: String {
        switch self {
        case .premiere:
            return "/v3/lists/estrenos-imprescindibles-en-taquilla/contents"
        case .free:
            return "/v3/lists/peliculas-gratis/contents/"
        case .story:
            return "/v3/lists/free-rakuten-tv-stories/contents/"
        case .subscription:
            return "/v3/lists/lo-mejor-de-tu-suscripcion/contents/"
        case .rent:
            return "/v3/lists/peliculas-mas-vistas-en-alquiler/contents/"
        case .buy:
            return "/v3/lists/peliculas-populares-en-compra/contents/"
        case .cartoon:
            return "/v3/lists/aventuras-de-animacion-en-alquiler/contents/"
        case .princess:
            return "/v3/lists/princesas-disney/contents/"
        case .missed:
            return "/v3/lists/si-te-perdiste/contents/"
        }
    }
    
    func makeRequestWith(page: Int) -> URLRequest? {
        let host = "https://gizmo.rakuten.tv"
        var components = URLComponents(string: host)!
        components.path = self.path
        components.queryItems = [
            URLQueryItem(name: "classification_id", value: "6"),
            URLQueryItem(name: "device_identifier", value: "ios"),
            URLQueryItem(name: "locale", value: "es"),
            URLQueryItem(name: "market_code", value: "es"),
            URLQueryItem(name: "page", value: String(page)),
        ]
        
        guard let url = components.url else { return nil }
        return URLRequest(url: url)
    }
}

extension ItemType: Comparable {
    static func < (lhs: ItemType, rhs: ItemType) -> Bool {
        return lhs.rawValue < rhs.rawValue
    }
}
