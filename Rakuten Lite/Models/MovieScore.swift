//
//  MovieScore.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 23/11/2019.
//  Copyright © 2019 me. All rights reserved.
//

import Foundation

struct MovieScore {
    let score: Double
    let votes: String?
    let iconUrl: URL?
}

extension MovieScore: Decodable {
    enum CodingKeys: String, CodingKey {
        case score
        case site
        case votes = "formatted_amount_of_votes"
        
        enum SiteKeys: String, CodingKey {
            case iconUrl = "image"
        }
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.score = try container.decode(Double.self, forKey: .score)
        self.votes = try? container.decode(String.self, forKey: .votes)
        
        let siteContainer = try container.nestedContainer(keyedBy: CodingKeys.SiteKeys.self, forKey: .site)
        self.iconUrl = try? siteContainer.decode(URL.self, forKey: .iconUrl)
    }
}

