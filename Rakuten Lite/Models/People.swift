//
//  People.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 25/11/2019.
//  Copyright © 2019 me. All rights reserved.
//

import Foundation

struct People {
    let name: String
    let photoUrl: URL?
}

extension People: Decodable {
    enum CodingKeys: String, CodingKey {
        case name
        case photoUrl = "photo"
    }
}


