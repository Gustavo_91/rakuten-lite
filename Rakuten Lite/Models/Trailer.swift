//
//  Trailer.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 29/11/2019.
//  Copyright © 2019 me. All rights reserved.
//

import Foundation

struct Trailer {
    let url: URL
    
    static func makeRequestWith(id: String) -> URLRequest? {
        let host = "https://gizmo.rakuten.tv"
        var components = URLComponents(string: host)!
        components.path = "/v3/me/streamings/"
        components.queryItems = [
            URLQueryItem(name: "classification_id", value: "6"),
            URLQueryItem(name: "device_identifier", value: "ios"),
            URLQueryItem(name: "locale", value: "es"),
            URLQueryItem(name: "market_code", value: "es"),
        ]
        
        guard let url = components.url else { return nil }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-type")
        
        let json: [String: Any] = [
            "audio_language":"SPA",
            "audio_quality": "2.0",
            "content_id": "\(id)",
            "content_type": "movies",
            "device_serial": "device_serial_1",
            "device_stream_video_quality": "FHD",
            "player": "ios:PD-NONE",
            "subtitle_language":"MIS",
            "video_type": "trailer"
        ]
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: json) else { return nil }
        request.httpBody = jsonData
        
        return request
    }
}

extension Trailer: Decodable {
    enum CodingKeys: String, CodingKey {
        case data
        
        enum DataKeys: String, CodingKey {
            case stream = "stream_infos"
            
            enum StreamKeys: String, CodingKey {
                case url
            }
        }
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let dataContainer = try container.nestedContainer(keyedBy: CodingKeys.DataKeys.self,
                                                          forKey: .data)
        
        var streamsContainer = try dataContainer.nestedUnkeyedContainer(forKey: .stream)
        var urls = [URL]()
        while !streamsContainer.isAtEnd {
            let container = try streamsContainer.nestedContainer(keyedBy: CodingKeys.DataKeys.StreamKeys.self)
            let url = try container.decode(URL.self, forKey: .url)
            urls.append(url)
        }
    
        self.url = urls[0]
    }
}
