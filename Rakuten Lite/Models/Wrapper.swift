//
//  Wrapper.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 26/11/2019.
//  Copyright © 2019 me. All rights reserved.
//

import Foundation

struct Wrapper {
    let items: [MovieItem]
    let pages: Int
}

extension Wrapper: Decodable {
    enum CodingKeys: String, CodingKey {
        case items = "data"
        case meta
        
        enum MetaKeys: String, CodingKey {
            case pagination
            
            enum PaginationKeys: String, CodingKey {
                case pages = "total_pages"
            }
        }
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.items = try container.decode([MovieItem].self, forKey: .items)
        
        let metaContainer = try container.nestedContainer(keyedBy: CodingKeys.MetaKeys.self, forKey: .meta)
        let paginationContainer = try metaContainer.nestedContainer(keyedBy: CodingKeys.MetaKeys.PaginationKeys.self, forKey: .pagination)
        self.pages = try paginationContainer.decode(Int.self, forKey: .pages)
    }
}
