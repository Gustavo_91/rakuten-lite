//
//  Movie.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 23/11/2019.
//  Copyright © 2019 me. All rights reserved.
//

import Foundation

struct Movie {
    let id: String
    let title: String
    let originalTitle: String
    let year: Int
    let plot: String
    let duration: Int
    let countries: [String]
    let genres: [String]
    let casting: [People]
    let scores: [MovieScore]
    var snapshotUrl: URL?
    
    static func makeRequestWith(id: String) -> URLRequest? {
        let host = "https://gizmo.rakuten.tv"
        var components = URLComponents(string: host)!
        components.path = "/v3/movies/\(id)/"
        components.queryItems = [
            URLQueryItem(name: "classification_id", value: "6"),
            URLQueryItem(name: "device_identifier", value: "ios"),
            URLQueryItem(name: "locale", value: "es"),
            URLQueryItem(name: "market_code", value: "es"),
        ]
        
        guard let url = components.url else { return nil }
        return URLRequest(url: url)
    }
}

extension Movie: Decodable {
    enum CodingKeys: String, CodingKey {
        case data
        
        enum DataKeys: String, CodingKey {
            case id
            case title
            case originalTitle = "original_title"
            case year
            case plot = "short_plot"
            case duration
            case directors
            case countries
            case actors
            case genres
            case scores
            case images
            
            enum ImageKeys: String, CodingKey {
                case snapshot
            }
            
            enum CountryKeys: String, CodingKey {
                case name
            }
            
            enum GenreKeys: String, CodingKey {
                case name
            }
        }
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let dataContainer = try container.nestedContainer(keyedBy: CodingKeys.DataKeys.self, forKey: .data)
        
        self.id = try dataContainer.decode(String.self, forKey: .id)
        self.title = try dataContainer.decode(String.self, forKey: .title)
        self.originalTitle = try dataContainer.decode(String.self, forKey: .originalTitle)
        self.year = try dataContainer.decode(Int.self, forKey: .year)
        self.plot = try dataContainer.decode(String.self, forKey: .plot)
        self.duration = try dataContainer.decode(Int.self, forKey: .duration)
        self.scores = try dataContainer.decode([MovieScore].self, forKey: .scores)
        
        let imagesContainer = try dataContainer.nestedContainer(keyedBy: CodingKeys.DataKeys.ImageKeys.self, forKey: .images)
        self.snapshotUrl = try? imagesContainer.decode(URL.self, forKey: .snapshot)
        
        var countriesContainer = try dataContainer.nestedUnkeyedContainer(forKey: .countries)
        var countries = [String]()
        while !countriesContainer.isAtEnd {
            let container = try countriesContainer.nestedContainer(keyedBy: CodingKeys.DataKeys.CountryKeys.self)
            let country = try container.decode(String.self, forKey: .name)
            countries.append(country)
        }
        self.countries = countries
        
        var genresContainer = try dataContainer.nestedUnkeyedContainer(forKey: .genres)
        var genres = [String]()
        while !genresContainer.isAtEnd {
            let container = try genresContainer.nestedContainer(keyedBy: CodingKeys.DataKeys.GenreKeys.self)
            let genre = try container.decode(String.self, forKey: .name)
            genres.append(genre)
        }
        self.genres = genres
        
        let actors = try dataContainer.decode([People].self, forKey: .actors)
        let directors = try dataContainer.decode([People].self, forKey: .directors)
        self.casting = actors + directors
    }
}
