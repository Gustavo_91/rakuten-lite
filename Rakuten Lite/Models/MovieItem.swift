//
//  MovieItem.swift
//  Rakuten Lite
//
//  Created by Gustavo Pirela on 21/11/2019.
//  Copyright © 2019 me. All rights reserved.
//

import Foundation

struct MovieItem {
    let id: String
    let title: String
    let price: String
    var score: Double?
    var votes: String?
    let artworkUrl: URL
    let snapshotUrl: URL
}

extension MovieItem: Decodable {
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case price = "label"
        case images
        case scoreInfo = "highlighted_score"
        
        enum ImagesKeys: String, CodingKey {
            case artwork
            case snapshot
        }
        
        enum ScoreKeys: String, CodingKey {
            case score
            case votes = "formatted_amount_of_votes"
        }
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.title = try container.decode(String.self, forKey: .title)
        self.price = try container.decode(String.self, forKey: .price)
        
        let imagesContainer = try container.nestedContainer(keyedBy: CodingKeys.ImagesKeys.self, forKey: .images)
        self.artworkUrl = try imagesContainer.decode(URL.self, forKey: .artwork)
        self.snapshotUrl = try imagesContainer.decode(URL.self, forKey: .snapshot)
        
        let scoreContainer = try container.nestedContainer(keyedBy: CodingKeys.ScoreKeys.self, forKey: .scoreInfo)
        self.score = try? scoreContainer.decode(Double.self, forKey: .score)
        self.votes = try? scoreContainer.decode(String.self, forKey: .votes)
    }
}
